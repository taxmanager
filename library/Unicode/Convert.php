<?php

class Unicode_Convert
{
	public static function removeDiacritics( $text, $encoding = 'utf-8' )
	{
		if ( $encoding != 'utf-8' )
		{
			$text = self::convertToUTF8( $text, $encoding );
		}
	}
	
	public static function convertToUTF8( $text, $encoding )
	{
		
	}
	
	public static function convertFromUTF8( $text, $encoding )
	{
		
	}
	
	public static function convert( $text, $encoding_from = 'utf-8', $encoding_to = 'utf-8' )
	{
		if ($encoding_from != 'utf-8')
		{
			$text = self::convertToUTF8( $text, $encoding_from );
		}
		
		if ($encoding_to != 'utf-8')
		{
			$text = self::convertFromUTF8( $text, $encoding_to );
		}
		
		return $text;
	}
};
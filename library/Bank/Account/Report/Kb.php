<?php

class Bank_Account_Report_Kb extends Bank_Account_Report_Abstract
{
	public function importReport( $file )
	{
		$data = $this -> parseReport( $file );
		var_dump($data);
	}
	
	public function parseReport( $file )
	{
		$out = Array();
		$f = file_get_contents($file);
		$lines = Explode("\n", $f);
		
		$data = Array();
		$in_block = false;
		$line_cnt = 0;
		foreach ($lines as $line)
		{
			$line = rtrim($line);
			if ($line == '')
			{ continue; }
			
			if ( preg_match(";^_+\$;", $line) )
			{
				if ($in_block)
				{
					$out[] = $data;
				}
				$in_block = false;
				$data = Array();
				$line_cnt = 0;
				continue;
			}
			
			$line_cnt++;
			
			if (($line_cnt == 1) && preg_match(';^((([0-9]+)-)?([0-9]+))?/([0-9]+);', $line, $M))
			{
				$in_block = true;
				$data['predcisli']  = $M[3];
				$data['cislo_uctu'] = $M[4];
				$data['banka']      = $M[5];
				
				$data['vs'] = SPrintF("%010d", (int) Trim(substr($line, 31, 10)) );
				
				preg_match(';([+-])?([0-9]+),([0-9]+) ([A-Z]{3});i', SubStr($line, 41, 29), $C);
				
				if ($C[1] == '')
				{
					$zn = -1;
				} else {
					$zn = ($C[1] == '-' ? -1 : 1);
				}
				$data['castka'] = ((float) (((int) $C[2]) + (((int) $C[3]) / 100))) * $zn;
				
				$data['datum_prijeti'] = strtotime( substr($line, 70, 10) );
				$data['datum_prijeti_x'] = Date('Y-m-d', $data['datum_prijeti']);
			}
			
			if ($line_cnt == 2)
			{
				$data['typ'] = Trim(substr($line, 0, 31));
				
				$data['ks'] = SPrintF("%04d", (int) Trim(substr($line, 31, 4)) );
				$data['datum_splatnosti'] = strtotime( substr($line, 70, 10) );
			}
			
			if ($line_cnt == 3)
			{
				$data['id'] = Trim(substr($line, 0, 31));
				
				$data['ss'] = SPrintF("%010d", (int) Trim(substr($line, 31, 10)) );
				$data['datum_zauctovani'] = strtotime( substr($line, 70, 10) );
			}
			
			if ($line_cnt == 4)
			{
				$data['popis_prikazce'] = Trim( substr( $line, 31, 49 ) );
			}
			
			if ($line_cnt == 5)
			{
				$data['popis_prijemce'] = Trim( substr( $line, 31, 49 ) );
			}
			
			if ($line_cnt > 5)
			{
				$data['popis_system'] = isset($data['popis_system']) ? $data['popis_system'] : '';
				$data['popis_system'] .= ($data['popis_system'] != "" ? "\n" : "").Trim( substr( $line, 31, 49 ) );
			}
		}
		
		return $out;
	}
};
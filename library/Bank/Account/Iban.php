<?php

class Bank_Account_Iban
{
	var $_data;

	public function __construct()
	{
		$this -> _data = Array();
		$data = file_get_contents(dirname(__FILE__).'/iban_format.dat');
		
		$lines = Explode("\n", $data);
		foreach ( $lines as $line )
		{
			$line = Trim( preg_replace(';[^a-zA-Z0-9];','', $line ) );
			if ($line)
			{
				$this -> _data[ SubStr($line, 0, 2) ] = $line;
			}
		}
		
	}
	
	public function normalizeIBAN( $iban )
	{
		return preg_replace( ';[^A-Z0-9];', '', StrToUpper( Trim($iban) ) );
	}
	
	protected function IBANtoNumber($iban)
	{
		$first = SubStr($iban, 0, 4);
		$second = SubStr($iban, 4);
		
		$number_ = $second.$first;
		$number = '';
		
		for ( $i = 0; $i < strlen($number_); $i++ )
		{
			$digit = SubStr( $number_, $i, 1 );
			
			if ($digit >= 'A' && $digit <= 'Z')
			{
				$number .= (Ord($digit) - 65 + 10);
			} else {
				$number .= $digit;
			}
		}
		
		return $number;
	}
	
	protected function countCheckDigit( $iban )
	{
		$to_check = $this -> IBANtoNumber($iban);
		
		$last = '';
		while ($to_check != '')
		{
			$pivot = min( strlen($to_check), 9 - strlen($last) );
			$part = SubStr( $to_check, 0, $pivot );
			$to_check = SubStr( $to_check, $pivot );
			
			$last = (string) (((int) ($last.$part)) % 97);
		}
		
		return (int) $last;
	}
	
	public function checkIBAN( $iban )
	{
		$iban = $this -> normalizeIBAN($iban);
		
		$country = SubStr($iban, 0, 2);
		
		if (!isset($this -> _data[$country]) || (strlen($this->_data[$country]) != strlen($iban)))
		{
			return false;
		}
		
		if ($this -> countCheckDigit($iban) == 1)
		{
			return true;
		} else {
			return false;
		}
	}
	
	public function finalizeIBAN( $iban_prototype )
	{
		$iban = $this -> normalizeIBAN($iban_prototype);
		
		$remainder = $this -> countCheckDigit( $iban );
		
		$check_digit = 98-$remainder;
		
		return SubStr($iban,0,2).SprintF("%02d", $check_digit).SubStr($iban,4);
	}
	
	public function parseIBAN( $iban )
	{
		$iban = $this -> normalizeIBAN($iban);
		if ( !$this -> checkIBAN( $iban ) )
		{
			return null;
		}
		
		$out = Array();
		$out['country'] = SubStr($iban, 0, 2);
		$mask = $this -> _data[$out['country']];
		
		for ($i = 2; $i < strlen($mask); $i++)
		{
			$out[SubStr($mask, $i, 1)] .= SubStr($iban, $i, 1);
		}
		
		return $out;
	}
	
	public function convertToIBAN( $account )
	{
		$mask = $this -> _data[$account['country']];
		
		$counts = Array();
		
		for ($i = 4; $i < strlen($mask); $i++)
		{
			$counts[SubStr($mask, $i, 1)]++;
		}
		
		$data = Array();
		foreach ( $counts as $k => $v )
		{
			$data[$k] =  str_pad( $account[ $k ], $v, '0', STR_PAD_LEFT);
		}
		
		$iban = $account['country'].'00';
		$counts = Array();
		for ($i = 4; $i < strlen($mask); $i++)
		{
			$k = SubStr($mask, $i, 1);
			$counts[$k] = (int) $counts[$k];
			
			$iban .= substr( $data[$k], $counts[$k], 1 );
			
			$counts[$k]++;
		}
		
		return $this -> finalizeIBAN( $iban );
	}
	
	public function prettyIBAN( $iban )
	{
		return preg_replace( ';(.{4});', '$1 ', $iban );
	}
};
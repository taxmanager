<?php

class Unicode
{
	public static function decodeUnicodeCategory($category)
	{
		$cat = Array(
			'Lu' => 1,
			'Ll' => 2,
			'Lt' => 3,
			'Lm' => 4,
			'Lo' => 5,
			'Mn' => 6,
			'Mc' => 7,
			'Me' => 8,
			'Nd' => 9,
			'Nl' => 10,
			'No' => 11,
			'Pc' => 12,
			'Pd' => 13,
			'Ps' => 14,
			'Pe' => 15,
			'Pi' => 16,
			'Pf' => 17,
			'Po' => 18,
			'Sm' => 19,
			'Sc' => 20,
			'Sk' => 21,
			'So' => 22,
			'Zs' => 23,
			'Zl' => 24,
			'Zp' => 25,
			'Cc' => 26,
			'Cf' => 27,
			'Cs' => 28,
			'Co' => 29,
			'Cn' => 30
		);
		
		$cat = array_flip($cat);
		
		return $cat[$category];
	}
	
	protected static function getUTFchar( &$string, &$unicode_character )
	{
		$MASKS = Array(
			'UTF_ONE_CHAR_MASK'			=>	0x80,
			'UTF_ONE_CHAR_VALUE'		=>	0x00,
			
			'UTF_TWO_CHARS_MASK'		=>	0xE0,
			'UTF_TWO_CHARS_VALUE'		=>	0xC0,
			
			'UTF_THREE_CHARS_MASK'		=>	0xF0,
			'UTF_THREE_CHARS_VALUE'		=>	0xE0,
			
			'UTF_FOUR_CHARS_MASK'		=>	0xF8,
			'UTF_FOUR_CHARS_VALUE'		=>	0xF0,
			
			'UTF_ADDITIONAL_CHAR_MASK'	=>	0xC0,
			'UTF_ADDITIONAL_CHAR_VALUE'	=>	0x80
		);
		
		$character = 0;
		$char_parts = 0;
		
		
		$c = substr( $string, 0, 1 );
		$string = substr( $string, 1 );
		
		// Input end
		if ( $c == "" )
		{ return false; }
		
		$c = ord( $c );
	
		// One character uchar
		if ( ($c & $MASKS['UTF_ONE_CHAR_MASK']) == $MASKS['UTF_ONE_CHAR_VALUE'] )
		{
			$unicode_character = $c;
			$char_parts = 1;
		}
		// Two characters uchar
		else if ( ($c & $MASKS['UTF_TWO_CHARS_MASK']) == $MASKS['UTF_TWO_CHARS_VALUE'] )
		{
			$unicode_character = ($c & ~$MASKS['UTF_TWO_CHARS_MASK']) << 6;
			$char_parts = 2;
		} 
		// Three characters uchar
		else if ( ($c & $MASKS['UTF_THREE_CHARS_MASK']) == $MASKS['UTF_THREE_CHARS_VALUE'] )
		{
			$unicode_character = ($c & ~$MASKS['UTF_THREE_CHARS_MASK']) << 12;
			$char_parts = 3;
		}
		// Four characters uchar
		else if ( ($c & $MASKS['UTF_FOUR_CHARS_MASK']) == $MASKS['UTF_FOUR_CHARS_VALUE'] )
		{
			$unicode_character = ($c & ~$MASKS['UTF_FOUR_CHARS_MASK']) << 18;
			$char_parts = 4;
		}
		// Unexpected char - error
		else 
		{
			return false;
		}
	
		for ($i = $char_parts-1; $i > 0; --$i)
		{
			$c = substr( $string, 0, 1 );
			$string = substr( $string, 1 );
			
			// Input end
			if ( $c == "" )
			{ return false; }
			
			$c = ord( $c );
			
			// Unexpected char - error
			if ( ($c & $MASKS['UTF_ADDITIONAL_CHAR_MASK']) != $MASKS['UTF_ADDITIONAL_CHAR_VALUE'] )
			{ return false; }
	
			$unicode_character |= ($c & ~$MASKS['UTF_ADDITIONAL_CHAR_MASK']) << (($i-1)*6);
		}
		
		return true;
	}
	
	protected static function Utf8ToUnicode( $utf_string )
	{
		$unicode = Array();
		
		$c = 0;
		while ( self::getUTFchar( $utf_string, $c ) )
		{
			$unicode[] = $c;
		}
		
		return $unicode;
	}
	
	
	public static function encodeUTF8Char( $unicode_char )
	{
		
	}
	
	public static function decodeUTF8Char( $utf8_char )
	{
		if ( self::getUTFchar( $utf8_char, $unicode ) )
		{
			return $unicode;
		}
		
		return null;
	}
	
	public static function getCharProperties($unicode_char, $F = null)
	{
		$record_length = 1 + 4 + 2 + 2;
		
		if (is_null($F))
		{
			$f = fopen(dirname(__FILE__).'/../data/unicode/ucd.dat', 'rb');
		} else {
			$f = $F;
		}
		
		fseek( $f, $unicode_char * $record_length );
		
		$data = fread( $f, $record_length );
		$decoded = unpack( "ccategory/Iindex/nupper/nlower", $data );
		
		if (is_null($F))
		{
			fclose($f);
		}
		
		return $decoded;
	}
	
	public static function decomponeUnicodeChar( $unicode_char )
	{
		$out = Array();
		
		$g = fopen(dirname(__FILE__).'/../data/unicode/ucd_decompose.dat', 'rb');
		
		$decoded = self::getCharProperties( $unicode_char );
	
		fseek( $g, $decoded['index'] );
		$data = fread($g, 1);
		$dec = unpack('clen', $data);
		
		if ( $dec['len'] > 0 )
		{
			$data = fread( $g, $dec['len'] );
			$out = unpack('n*', $data);
		}
		
		fclose($g);
		
		return $out;
	}
	
	public static function toUpper( $text )
	{
		
	}
	
	public static function toLower( $text )
	{
		
	}
};
<?php

final class Plugin_Routing extends Zend_Controller_Plugin_Abstract
{
	public function routeStartup( Zend_Controller_Request_Abstract $request )
	{
		$frontController = Zend_Controller_Front::getInstance();
		$router = $frontController -> getRouter(); 
		
		/*
		 * ----------------------------------------------------------
		 *  COMPETITIONS
		 * ---------------------------------------------------------- 
		 */
		
		$route = new Zend_Controller_Router_Route_Regex(
												    'zavody/(\d+)-(.+)\.html',
												    array(
												        'controller' => 'competitions',
												        'action'     => 'show'
												    ),
												    array(
												        1 => 'competition',
												        2 => 'seoname'
												    ),
												    'zavody/%d-%s.html'
												);
		$router->addRoute('competitions_show', $route);
		
		$route = new Zend_Controller_Router_Route(
												    'zavody',
												    array(
												        'controller' => 'competitions',
												        'action'     => 'index'
												    )
												);
		$router->addRoute('competitions_index', $route);
		
		
		$route = new Zend_Controller_Router_Route(
												    'program',
												    array(
												        'controller' => 'info',
												        'action'     => 'show',
												        'page' => 'program')
												);
		$router->addRoute('program_info', $route);
		
		$route = new Zend_Controller_Router_Route(
												    'o-webu',
												    array(
												        'controller' => 'info',
												        'action'     => 'show',
												        'page' => 'o-webu')
												);
		$router->addRoute('owebu_info', $route);
		
		/*
		 * ----------------------------------------------------------
		 *  CLUBS
		 * ---------------------------------------------------------- 
		 */
		
		$route = new Zend_Controller_Router_Route_Regex(
												    'oddily/(\d+)-(.+)\.html',
												    array(
												        'controller' => 'clubs',
												        'action'     => 'show'
												    ),
												    array(
												        1 => 'club',
												        2 => 'seoname'
												    ),
												    'oddily/%d-%s.html'
												);
		$router->addRoute('clubs_show', $route);
		
		$route = new Zend_Controller_Router_Route(
												    'oddily',
												    array(
												        'controller' => 'clubs',
												        'action'     => 'index'
												    )
												);
		$router->addRoute('clubs_index', $route);
		
		/*
		 * ----------------------------------------------------------
		 *  PEOPLE
		 * ---------------------------------------------------------- 
		 */
		
		$route = new Zend_Controller_Router_Route_Regex(
												    'lide/(\d+)-(.+)\.html',
												    array(
												        'controller' => 'people',
												        'action'     => 'show'
												    ),
												    array(
												        1 => 'person',
												        2 => 'seoname'
												    ),
												    'lide/%d-%s.html'
												);
		$router->addRoute('people_show', $route);
		
		$route = new Zend_Controller_Router_Route(
												    'lide',
												    array(
												        'controller' => 'people',
												        'action'     => 'index'
												    )
												);
		$router->addRoute('people_index', $route);
		
	}
}

<?php

final class Plugin_Initialization extends Zend_Controller_Plugin_Abstract
{
	public function routeStartup( Zend_Controller_Request_Abstract $request )
	{
		$config = new Zend_Config_Ini(dirname(__FILE__).'/../../config/config.ini', null);
		Zend_Registry::set( 'config', $config );
		
		Zend_Registry::set( 'active_page', 'homepage' );
		Zend_Registry::set( 'page_title', '' );
	}
}

<?php

 final class Plugin_Database extends Zend_Controller_Plugin_Abstract
 {
  
 	/**
 	 * The entry point of Database plugin in the startup
 	 *
 	 * @param Zend_Controller_Request_Abstract $request
 	 */
  public function routeStartup(Zend_Controller_Request_Abstract $request)
  {
   if (Zend_Registry::isRegistered('config'))
   {
    $config = Zend_Registry::get('config');
    $db = Zend_Db::factory($config->database);
    
    Zend_Db_Table_Abstract::setDefaultAdapter($db);

    Zend_Registry::set('db',$db);
   }
   
   else
   {
    // Error handling.
   }
  }
  
 }



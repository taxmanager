<?php

class IndexController extends Zend_Controller_Action 
{
	public function indexAction()
	{
		$u = Unicode::decodeUTF8Char('č');
		
		$data = Unicode::decomponeUnicodeChar( $u );

		foreach ( $data as $d )
		{
			$props = Unicode::getCharProperties( $d );
			
			echo Unicode::decodeUnicodeCategory( $props['category'] ).' ';
		}
		
		//$bank_report = new Bank_Account_Report_Kb();
		
		//$bank_report-> importReport( 'C:\\development\\taxmanager\\unittesting\\vypis.txt' );
	}
};
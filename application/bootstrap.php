<?php 
 
/* Report all errors directly to the screen for simple diagnostics in the dev environment */  
error_reporting(E_ALL | E_STRICT);  
ini_set('display_startup_errors', 1);  
ini_set('display_errors', 1); 
 
/* Add the Zend Framework library to the include path so that we can access the ZF classes */ 
set_include_path(dirname(__FILE__).'/../library' . PATH_SEPARATOR . dirname(__FILE__).'/../texts' . PATH_SEPARATOR . get_include_path());

define('DATA_DIR', dirname(__FILE__).'/../public/data');

date_default_timezone_set( 'Europe/Prague' );
 
/* Set up autoload so we don't have to explicitely require each Zend Framework class */ 
require_once "Zend/Loader.php"; 
Zend_Loader::registerAutoload(); 

Zend_Layout::startMVC();

/* Set the singleton instance of the front controller */ 
$frontController = Zend_Controller_Front::getInstance(); 
/* Disable error handler so it doesn't intercept all those errors we enabled above */ 
$frontController->throwExceptions(true); 
/* Point the front controller to your action controller directory */ 

$frontController->addModuleDirectory(dirname(__FILE__).'/modules');

/*
$frontController->setControllerDirectory(
	array(
    'default' => dirname(__FILE__).'/default/controllers',
    'faktury'    => dirname(__FILE__).'/faktury/controllers',
    'nastaveni'    => dirname(__FILE__).'/nastaveni/controllers'
)); 
*/
$frontController->registerPlugin( new Plugin_Initialization() );
$frontController->registerPlugin( new Plugin_Database() );
$frontController->registerPlugin( new Plugin_Routing() );

//$frontController->setBaseUrl('/robportal/public');

Zend_Registry::set( 'map_atlas', false );

/* OK, do your stuff, front controller */ 
$frontController->dispatch(); 
